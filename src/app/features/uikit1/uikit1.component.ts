import { Component, OnInit } from '@angular/core';
import { TabbarComponent } from '../../shared/tabbar.component';

interface Country {
  id: number;
  name: string;
  desc: string;
  cities: City[];
}
interface City {
  id: number;
  name: string;
  desc: string;
}

@Component({
  selector: 'ls-uikit1',
  template: `
    <ls-tabbar 
      [items]="countries" 
      [active]="activeCountry"
      (tabClick)="setCountry($event)"
    ></ls-tabbar>

    <ls-tabbar
      *ngIf="activeCountry"
      [active]="activeCity"
      [items]="activeCountry.cities"
      (tabClick)="setCity($event)"
    ></ls-tabbar>
    
    <div class="alert alert-danger" *ngIf="activeCity">
      {{activeCity.desc}}
    </div>
    
    <hr>


    <ls-card
      title="Profile"
      headerCls="success" [marginBottom]="true"
      icon="fa fa-facebook"
      (iconClick)="openUrl('http://www.facebook.com')"
    >
      <div class="row">
        <div class="col">
          <ls-card title="left">...</ls-card>
        </div>
        <div class="col">
          <ls-card title="right">...</ls-card>
        </div>
      </div>
    </ls-card>

    <ls-card
      title="Newsletter"
      icon="fa fa-google"
      url="http://www.google.com"
      (iconClick)="doSomething()"
    >
      <input type="text">
      <input type="text">
      <input type="text">
      <button>click me</button>

    </ls-card>
  `,
  styles: [
  ]
})
export class Uikit1Component  {
  activeCountry: Country | null = null;
  activeCity: City | null = null;
  countries: Country[] = [];

  constructor() {
    setTimeout(() => {
      this.countries =  [
        {
          id: 11,
          name: 'Italy',
          desc: 'bla bla italy',
          cities: [
            { id: 111, name: 'Rome', desc: 'lorem ipsum Rome' },
            { id: 222, name: 'Milano', desc: 'lorem ipsum Milano' },
            { id: 333, name: 'Palermo', desc: 'lorem ipsum Palermo' },
          ]
        },
        {
          id: 22,
          name: 'Germany',
          desc: 'bla bla Germany',
          cities: [
            { id: 14, name: 'Berlin', desc: 'lorem ipsum Berlin' },
            { id: 234, name: 'Monaco', desc: 'lorem ipsum Monaco' },
          ]
        },
        {
          id: 33,
          name: 'Spain',
          desc: 'bla bla spain',
          cities: [
            { id: 333141, name: 'Madrid', desc: 'lorem ipsum Matridd' },
          ]
        },

      ];
      this.setCountry(this.countries[0])
    }, 100)

  }

  setCountry(country: Country) {
    this.activeCountry = country;
    this.activeCity =  this.activeCountry.cities[0];

  }

  setCity(city: City) {
    this.activeCity = city;
  }




  openUrl(url: string) {
    console.log('open url')
    window.open(url)
    /*  if (this.url) {

      }*/
  }

  doSomething() {
    console.log('do something')
  }
}

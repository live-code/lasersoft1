import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';
import { Language, LanguageService } from '../../core/services/language.service';

@Component({
  selector: 'ls-settings',
  template: `
    <h1>  {{'themeMsg' | translate}} : {{themeService.theme}}</h1>

    <button (click)="themeService.setTheme('light')">light</button>
    <button (click)="themeService.setTheme('dark')">dark</button>

    <hr>
    <h1>current lang: {{languageService.language}}</h1>
    {{languageService.getValue('hello')}}
    {{'hello' | translate}}
    
    <button (click)="languageService.setLanguage('it')">It</button>
    <button (click)="languageService.setLanguage('en')">En</button>

    <hr>
    
   
    {{1636623457000 | date: 'dd/MM/yyyy hh:mm:ss'}}
    {{1.23232523523 | number: '1.2-4'}}

  `,
})
export class SettingsComponent {

  constructor(
    public themeService: ThemeService,
    public languageService: LanguageService
  ) {
    console.log(themeService.theme)
  }

  doSomething() {
  }

}

// eventi
// timer
// XHR

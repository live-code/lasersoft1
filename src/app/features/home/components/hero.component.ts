import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { HomeService } from '../services/home.service';

@Component({
  selector: 'ls-hero',
 // changeDetection: ChangeDetectionStrategy.OnPush,

  template: `
    <p>
      hero works! {{value}}
    </p>
    
    {{render()}}
  `,
  styles: [
  ]
})
export class HeroComponent  {
  @Input() value: number | null = null;

  render() {
    console.log('render hero')
  }
}

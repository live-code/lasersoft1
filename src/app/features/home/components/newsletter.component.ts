import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ls-newsletter',
  template: `
    <p>
      newsletter works! {{value}}
    </p>
    <button (click)="dec.emit()">-</button>
    <button (click)="inc.emit()">+</button>
    
    <input type="text" ngModel>
  `,
  styles: [
  ]
})
export class NewsletterComponent {
  @Input() value: number | null = null;
  @Output() dec = new EventEmitter();
  @Output() inc = new EventEmitter();
}

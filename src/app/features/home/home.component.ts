import { Component, OnInit } from '@angular/core';
import { HomeService } from './services/home.service';

@Component({
  selector: 'ls-home',
  template: `
    
    <ls-hero
      [value]="homeService.counter"
    ></ls-hero>
    <ls-news></ls-news>
    <ls-newsletter 
      [value]="homeService.counter"
      (inc)="homeService.inc()"
      (dec)="homeService.dec()"
    ></ls-newsletter>
  `,
})
export class HomeComponent {
  constructor(public homeService: HomeService) { }
}

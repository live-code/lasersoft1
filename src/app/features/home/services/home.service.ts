import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  counter = 1;

  constructor() { }

  dec() {
    this.counter--;
  }
  inc() {
    this.counter++;
  }
}


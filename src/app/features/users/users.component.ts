import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';

@Component({
  selector: 'ls-users',
  template: `
    <ls-users-errors [error]="error" *ngIf="error"></ls-users-errors>

    <ls-users-form
      [activeUser]="activeUser"
      (clear)="clear()"
      (save)="save($event)"
    > </ls-users-form>
  
    <hr>
    
    <ls-users-list
      [users]="users"
      [activeUser]="activeUser"
      (setActive)="setActive($event)"
      (delete)="deleteHandler($event)"
    ></ls-users-list>
    
  `,
})
export class UsersComponent {
  users: User[] = [];
  activeUser: User | null = null
  error: boolean = false;

  constructor(private http: HttpClient) {
    this.getUsers();

  }

  getUsers() {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe((res) => {
        this.users = res;
      })
  }

  deleteHandler(user: User) {
    this.error = false;
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${user.id}`)
      .subscribe(
        () => {
          this.users = this.users.filter(u => u.id !== user.id)
          if (user.id === this.activeUser?.id) {
            this.activeUser = null;
          }
        },
        () => {
          this.error = true;
        }
      )

  }

  save(user: User) {
    if (this.activeUser?.id) {
      this.edit(user)
    } else {
      this.add(user);
    }
  }

  add(user: User) {
    this.http.post<User>('https://jsonplaceholder.typicode.com/users', user)
      .subscribe(res => {
        this.users = [...this.users, res]
        this.clear();
      })
  }

  edit(user: User) {
    this.http.patch<User>(`https://jsonplaceholder.typicode.com/users/${this.activeUser?.id}`, user)
      .subscribe(res => {
        this.users  = this.users.map( (u) => {
          return u.id === this.activeUser?.id ? res : u;
        })
      })
  }

  setActive(user: User) {
    this.activeUser = user;
  }

  clear() {
    this.activeUser = {} as User;
  }
}

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'ls-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ls-users-list-item
      *ngFor="let user of users"
      [user]="user"
      [activeUser]="activeUser"
      (setActive)="setActive.emit($event)"
      (delete)="delete.emit($event)"
    ></ls-users-list-item>
  `,
})
export class UsersListComponent {
  @Input() users: User[] = [];
  @Input() activeUser: User | null = null;
  @Output() setActive = new EventEmitter<User>()
  @Output() delete = new EventEmitter<User>()
}

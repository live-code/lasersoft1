import { Component, Input } from '@angular/core';

@Component({
  selector: 'ls-users-errors',
  template: `
    <div class="alert alert-danger">c'è un errore!</div>
  `,
})
export class UsersErrorsComponent {
  @Input() error: boolean = false;

}

import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { User } from '../../../model/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'ls-users-form',
  template: `
    <form #f="ngForm" (submit)="save.emit(f.value)">

      <div *ngIf="inputName.errors?.required">il campo è obbligatorio</div>
      <div *ngIf="inputName.errors?.minlength">
        il campo deve avere almeno 3 chars. Ti mancano {{inputName.errors?.minlength.requiredLength - inputName.errors?.minlength.actualLength}} caratteri.
      </div>

      <input
        type="text" class="form-control"
        [ngClass]="{
          'is-valid': inputName.valid, 
          'is-invalid': inputName.invalid && f.dirty
        }"
        placeholder="add name"
        [ngModel]="activeUser?.name"
        #inputName="ngModel"
        #inputRef
        name="name"
        required minlength="3"
      >

      <input
        type="text" class="form-control is-invalid" placeholder="add email"
        [ngClass]="{'is-valid': inputEmail.valid, 'is-invalid': inputEmail.invalid && f.dirty}"
        [ngModel]="activeUser?.email"
        #inputEmail="ngModel"
        name="email"
        required
      >

      <button type="submit" [disabled]="f.invalid">
        {{activeUser?.id ? 'EDIT' : 'ADD'}}
      </button>
      <button type="button" (click)="clearHandler(f)" *ngIf="activeUser?.id">ADD ANOTHER</button>
      
    </form>
    
  `,
})
export class UsersFormComponent implements OnChanges, AfterViewInit {
  @Input() activeUser: User | null = null;
  @Output() clear = new EventEmitter()
  @Output() save = new EventEmitter<User>()
  @ViewChild('f') form!: NgForm;
  @ViewChild('inputRef') inputRef!: ElementRef<HTMLInputElement>

  ngOnChanges() {
    if (this.activeUser && !this.activeUser?.id) {
      this.form.reset()
    }
  }

  ngAfterViewInit() {
    this.inputRef.nativeElement.focus()
  }

  clearHandler(f: NgForm) {
    f.reset()
    this.clear.emit()
  }

}

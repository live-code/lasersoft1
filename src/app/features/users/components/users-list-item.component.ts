import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'ls-users-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      class="list-group-item"
      [ngClass]="{'active': user.id === activeUser?.id}"
      (click)="setActive.emit(user)"
    >
      {{user.name}} - {{user.email}}
      <div class="pull-right">
        <i class="fa fa-arrow-down"  (click)="toggleHandler($event)"></i>
        <i class="fa fa-trash"  (click)="deleteHandler(user, $event)"></i>
      </div>

      <div *ngIf="isOpened">
        {{user.address.street}} <br>
        {{user.address.city}} <br>
        {{user.address.zipcode}} <br>
        
        <ls-mapquest 
          [lat]="user.address.geo.lat" 
          [lng]="user.address.geo.lng"
          [zoom]="zoom"
        ></ls-mapquest>     
        
        <button (click)="dec($event)">-</button>
        <button (click)="inc($event)">+</button>
      </div>
    </li>
    
    {{render()}}
  `,
})
export class UsersListItemComponent  {
  @Input() user!: User;
  @Input() activeUser: User | null = null;
  @Output() setActive = new EventEmitter<User>()
  @Output() delete = new EventEmitter<User>()
  zoom = 4;
  isOpened = false;

  toggleHandler(event: MouseEvent) {
    event.stopPropagation()
    this.isOpened = !this.isOpened
  }

  deleteHandler(user: User, event: MouseEvent) {
    event.stopPropagation()
    this.delete.emit(user)
  }

  dec(event: MouseEvent) {
    event.stopPropagation();
    if (this.zoom > 1) {
      this.zoom = this.zoom - 1
    }
  }
  inc(event: MouseEvent) {
    event.stopPropagation();
    if (this.zoom < 18) {
      this.zoom = this.zoom + 1
    }
  }

  render() {
    console.log('render: ListItem')
  }
}

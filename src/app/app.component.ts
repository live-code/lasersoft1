import { Component } from '@angular/core';
import { LanguageService } from './core/services/language.service';
import { translations } from './core/services/translations';

@Component({
  selector: 'ls-root',
  template: `
    <ls-navbar></ls-navbar>
    
    <div class="container mt-3">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {

  constructor(public languageService: LanguageService) {
    // http.get(..)
    languageService.setLanguage('it')
    languageService.setTranslations(translations)
  }

}

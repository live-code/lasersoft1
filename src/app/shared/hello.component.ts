import { Component, Input } from '@angular/core';

@Component({
  selector: 'ls-hello',
  template: `
    <h1>Hello {{name}}</h1>
    {{value}}
  `
})
export class HelloComponent {
  @Input() name: string = 'Guest';
  @Input() value: number | string = 0;
}

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'ls-mapquest',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <img
      width="100%"
      [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center='+ lat + ',' +  lng + '&size=600,400&zoom=' + zoom" alt="">
    
    {{render()}}
  `,
})
export class MapquestComponent {
  @Input() lat: string | null = null;
  @Input() lng: string | null = null;
  @Input() zoom: number = 5;

  render() {
    console.log('render: MapQuest')
  }
}

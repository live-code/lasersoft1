import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ls-card',
  template: `
    <div class="card" [ngClass]="{'mb-3': marginBottom}">
      <div 
        class="card-header"
        [ngClass]="{
          'bg-success text-white': headerCls === 'success',
          'bg-warning': headerCls === 'warning'
        }"
        *ngIf="title"
        (click)="isOpened = !isOpened"
      >
        {{title}}
        
        <i class="pull-right" *ngIf="icon" 
           (click)="iconClickHandler($event)">
          <i [class]="icon"></i>
        </i>
      </div>
      
      <div class="card-body" *ngIf="isOpened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: string | null = null;
  @Input() headerCls: 'success' | 'warning' | null = null
  @Input() icon: string | null = null;
  @Input() marginBottom: boolean = false;
  @Output() iconClick = new EventEmitter()
  isOpened = true;

  iconClickHandler(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit()
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ls-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item" 
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a class="nav-link" [ngClass]="{'active': item.id === active?.id}">
          {{item.name}}
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent<T extends { id: number, name: string}> {
  @Input() items: T[] = [];
  @Output() tabClick = new EventEmitter<T>();
  @Input() active: T | null = null;
}

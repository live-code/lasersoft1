export const translations: { [key: string]: any } = {
  it: {
    hello: 'ciao!',
    themeMsg: 'il tema corrente è '
  },
  en: {
    hello: 'Hi!',
    themeMsg: 'Current theme is '
  },
}

import { Injectable } from '@angular/core';

export type Theme = 'dark' | 'light';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private value: Theme = 'dark';

  constructor() {
    this.value = localStorage.getItem('theme') as Theme || 'dark';
  }

  setTheme(val: Theme) {
    localStorage.setItem('theme', val)
    this.value = val;
  }

  get theme(): Theme {
    // ...
    return this.value;
  }

}

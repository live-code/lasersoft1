import { Pipe, PipeTransform } from '@angular/core';
import { LanguageService } from '../services/language.service';

@Pipe({
  name: 'translate',
  pure: false,
})
export class TranslatePipe implements PipeTransform {

  constructor(private languageService: LanguageService) {
  }

  transform(val: string): string | null {
    return this.languageService.getValue(val);
  }

}


import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'ls-navbar',
  template: `
    <div 
      class="p-3" 
      [ngClass]="{
        'bg-dark': themeService.theme === 'dark',
        'bg-light': themeService.theme === 'light'
      }"
    >
      <button routerLink="login">login</button>
      <button [routerLink]="'home'">home</button>
      <button routerLink="settings">settings</button>
      <button routerLink="users">users</button>
      <button routerLink="uikit1">uikit1</button>
      {{themeService.theme}}
      {{'hello' | translate}}
    </div>
    
    <hr>

  `,
})
export class NavbarComponent {

  constructor(public themeService: ThemeService) {

  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HelloComponent } from './shared/hello.component';
import { UsersComponent } from './features/users/users.component';
import { LoginComponent } from './features/login/login.component';
import { HomeComponent } from './features/home/home.component';
import { SettingsComponent } from './features/settings/settings.component';
import { RouterModule } from '@angular/router';
import { ThemeService } from './core/services/theme.service';
import { NavbarComponent } from './core/components/navbar.component';
import { TranslatePipe } from './core/pipes/translate.pipe';
import { HeroComponent } from './features/home/components/hero.component';
import { NewsComponent } from './features/home/components/news.component';
import { NewsletterComponent } from './features/home/components/newsletter.component';
import { CardComponent } from './shared/card.component';
import { Uikit1Component } from './features/uikit1/uikit1.component';
import { TabbarComponent } from './shared/tabbar.component';
import { UsersErrorsComponent } from './features/users/components/users-errors.component';
import { UsersFormComponent } from './features/users/components/users-form.component';
import { UsersListComponent } from './features/users/components/users-list.component';
import { UsersListItemComponent } from './features/users/components/users-list-item.component';
import { MapquestComponent } from './shared/mapquest.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    UsersComponent,
    LoginComponent,
    HomeComponent,
    SettingsComponent,
    NavbarComponent,
    TranslatePipe,
    HeroComponent,
    NewsComponent,
    NewsletterComponent,
    CardComponent,
    Uikit1Component,
    TabbarComponent,
    UsersErrorsComponent,
    UsersFormComponent,
    UsersListComponent,
    UsersListItemComponent,
    MapquestComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: 'home', component: HomeComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'uikit1', component: Uikit1Component },
      { path: 'users', component: UsersComponent },
      { path: '**', redirectTo: 'login' },
    ])
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
